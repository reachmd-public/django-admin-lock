from datetime import timedelta
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.utils.timezone import now
from django.core.exceptions import MultipleObjectsReturned
from .models import UserLock
from .conf import MAX_LOCK_TIME


def get_or_create_lock(obj, user):
    """
    Atomically check if there is a lock and acquire one if there is none
    return: :class:`adminlock.models.UserLock` object, True if there already exists a lock and
            :class:`adminlock.models.UserLock` object, False if a lock was just created
    """
    now_lock = now() - timedelta(seconds=MAX_LOCK_TIME)
    content_type = ContentType.objects.get_for_model(obj.__class__)

    with transaction.atomic():
        try:
            lock, created = UserLock.objects.get_or_create(
                content_type=content_type,
                object_pk=obj.pk,
                create_at__gte=now_lock,
                defaults={
                    'user': user,
                    'content_type': content_type,
                    'object_pk': obj.pk
                }
            )
        except MultipleObjectsReturned:
            lock = UserLock.objects.filter(
                content_type=content_type,
                object_pk=obj.pk,
                create_at__gte=now_lock,
            ).order_by('-id').first()
            created = False

    if not created and lock.user != user:
        return lock, True

    return lock, False
